//import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Word {
    private List<Pair<Character,Boolean>> chars = new ArrayList();

    Word(String wordIn) {
        for (char c: wordIn.toCharArray()) {
            chars.add(new ImmutablePair<Character, Boolean>(c, false));
        }
    }

    public static String displayChar(Pair<Character,Boolean> p) {
        if (p.getRight()) {
            return String.valueOf(p.getLeft());
        } else {
            return " _ ";
        }
    }

    public static Pair<Character,Boolean> setChar(char c, Pair<Character,Boolean> p) {
        if (p.getLeft() == c) {
            return new ImmutablePair(p.getLeft(), true);
        } else {
            return p;
        }

    }

    public long numOfGuessed() {
        return chars.stream().filter(p -> p.getRight()).collect(Collectors.counting());
    }

    public void setChars(char c) {
        chars = chars.stream().map(p -> Word.setChar(c, p))
                .collect(Collectors.toList());
    }

    public String display() {
        return String.join("", chars.stream().map(p -> Word.displayChar(p))
                .collect(Collectors.toList()));
    }

    @Override
    public String toString() {
        return "Word{" +
                "chars=" + chars +
                '}';
    }

    public long size() {
        return chars.size();
    }
}
