import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StoredWord {

    private List<Pair<Character,Boolean>> letters;
    public StoredWord(String wordIn) {

        letters = new ArrayList();
        for (Character c: wordIn.toCharArray()) {
            letters.add(new ImmutablePair(c, false));
        }
    }

    @Override
    public String toString() {
        return "StoredWord{" +
                "letters=" + letters +
                '}';
    }

    public String toStringWithBlanks(){
        String output = new String();
        for (Pair p: letters){
            if(p.getRight().equals(true)){
                output = output.concat(p.getLeft().toString());
            }
            else {
                output = output.concat(" _ ");
            }
        }
        return output;
    }

    public String toStringWithBlanks2() {
        List output =
                letters.stream()
                        .map(p -> StoredWord.showPair(p))
                        //.filter(p -> p.getLeft().equals('a'))
                        .collect(Collectors.toList());
        return String.join("",output);
    }

    public void test() {
        List<String> l = new ArrayList<>();
        l.add("pineapple");
        l.add("peach");
        l.add("apple");
        System.out.println(String.join(" ",l));
    }

    private static String showPair(Pair<Character,Boolean> p) {
        if (p.getRight().equals(true)) {
            return p.getLeft().toString();
        }
        else {
            return " _ ";
        }
    }
}
